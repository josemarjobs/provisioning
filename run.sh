# get the ip address
sudo apt-get update
sudo apt-get install build-essential git-core python-software-properties nodejs
sudo apt-get install curl
# install rbenv
curl https://raw.githubusercontent.com/fesplugas/rbenv-installer/master/bin/rbenv-installer | bash

# install vim
sudo apt-get install vim

# install sqlite
sudo apt-get install sqlite3 libsqlite3-dev 

# NGINX
sudo apt-add-repository ppa:nginx/stable
sudo apt-get update
sudo apt-get install nginx









