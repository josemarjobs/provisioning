class DashboardsController < ApplicationController
  before_filter :authenticate

  def show
    @dashboard = Dashboard.new(current_user)   
  end

  private

  def authenticate
    redirect_to new_session_path unless signed_in?
  end
end