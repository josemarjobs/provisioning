class ApplicationController < ActionController::Base
  include Monban::ControllerHelpers
  protect_from_forgery

  before_filter :create_shout

  private
  def create_shout
    @text_shout = TextShout.new
    @photo_shout = PhotoShout.new
  end
end
