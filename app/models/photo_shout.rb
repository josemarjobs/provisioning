class PhotoShout < ActiveRecord::Base
  has_attached_file :image, styles: {
    shout: '300x300>'
  }
end
